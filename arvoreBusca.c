#include <stdio.h>
#include <stdlib.h>

typedef struct arvore {
  int info;
  struct arvore *esq;
  struct arvore *dir;
} Arvore;

int buscar (Arvore *a, int v) {
  if (a == NULL) { return 0; } /*Nao achou*/
  else if (v < a->info) {
    return buscar (a->esq, v);
  }
  else if (v > a->info) {
    return buscar (a->dir, v);
  }
  else { return 1; } /*Achou*/
}

Arvore* inserir (Arvore *a, int v) {
  if (a == NULL) {
    a = (Arvore*)malloc(sizeof(Arvore));
    a->info = v;
    a->esq = a->dir = NULL;
  }
  else if (v < a->info) {
    a->esq = inserir (a->esq, v);
  }
  else { a->dir = inserir (a->dir, v); }
  return a;
}

void in_order(Arvore *a){
  if(!a)
    return;
  in_order(a->esq);
  printf("%d ",a->info);
  in_order(a->dir);
}

Arvore* remover(Arvore *a, int x){
  Arvore * aux, * pai_aux;
  int filhos = 0,tmp;

  if(!a)
    return(NULL);

  if(a->info < x)
    a->dir = remover(a->dir,x);
  else if(a->info > x)
    a->esq = remover(a->esq,x);
  else{
    if(a->esq)
      filhos++;
    if(a->dir)
      filhos++;

    if(filhos == 0){
      free(a);
      return(NULL);
    }
    else if(filhos == 1){
      aux = a->esq ? a->esq : a->dir;
      free(a);
      return(aux);
    }
    else{
      aux = a->esq;
      pai_aux = a;
      while(aux->dir){ pai_aux = aux; aux = aux->dir; }
      tmp = a->info;
      a->info = aux->info;
      aux->info = tmp;
      pai_aux->dir = remover(aux,tmp);
      return(a);
    }
  }

  return(a);
}

void print(Arvore * a,int spaces){
  int i;
  for(i=0;i<spaces;i++) printf(" ");
  if(!a){
    printf("\n");
    return;
  }

  printf("%d\n", a->info);
  print(a->esq,spaces+2);
  print(a->dir,spaces+2);
}

int eh_espelho(Arvore * arv_a, Arvore * arv_b){
    if (arv_a == NULL && arv_b == NULL) {
        return 1;
    }
    // Caso base: uma das �rvores � nula
    if (arv_a == NULL || arv_b == NULL) {
        return 0;
    }
    // Caso base: valor dos n�s diferentes
    if (arv_a->info != arv_b->info) {
        return 0;
    }
    // Verificar se sub-�rvores esquerda e direita s�o espelhos
    int i = eh_espelho(arv_a->esq, arv_b->dir) && eh_espelho(arv_a->dir, arv_b->esq);
    return i;
}

int arv_bin_check(Arvore * a){
    if (a == NULL) {
        return 1;
    }

    if (a->esq != NULL && a->esq->info >= a->info) {
        return 0;
    }

    if (a->dir != NULL && a->dir->info < a->info) {
        return 0;
    }

    int i = arv_bin_check(a->dir) && arv_bin_check(a->esq);
    return i;
}

int main(){
  Arvore * a, * b, * c, * c2;

  a = inserir(NULL,50);
  b = inserir(NULL,10);

  c = inserir(NULL, 20);

  c2 = inserir(NULL, 20);
    c2 = inserir(c2, 30);
    c2 = inserir(c2, 10);


  c->dir = b;
  c->esq = a;


  int p = arv_bin_check(c);
  if(p > 0){
    printf("\n E Uma Arvore De Busca! \n");
  }else{
    printf("\n Nao E Uma Arvore De Busca! \n");
  }

}


